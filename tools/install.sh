# script basado en https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh

WIFI_INTERFACE=$(cat /proc/net/wireless | perl -ne '/(\w+):/ && print $1')
AP_PSK=libreCast
USER_NAME=$(whoami)
OS_RELEASE_FILE=/etc/os-release

verificar_debian_deps() {
  echo "Verificando dependencias ..."

  hash apt >/dev/null 2>&1 || {
    echo "Error: apt no detectado"
    echo "el presente script funciona solo en sistemas"
    echo "basados en Debian"
    exit 1
  }

  hash sudo >/dev/null 2>&1 || {
    echo "Error: sudo no está instalado"
    echo "Por favor contactar el administrador"
    echo "del equipo para proceder con la instalación"
    exit 1
  }

  hash git >/dev/null 2>&1 || {
    echo "Instalando git ..."
    sudo apt install git
  }

  hash gst-launch-1.0  >/dev/null 2>&1 || {
    echo "Instalando gstreamer1.0-tools ..."
    sudo apt install gstreamer1.0-tools
  }

  hash pacmd  >/dev/null 2>&1 || {
    echo "Instalando pulseaudio ..."
    sudo apt install pulseaudio-utils
  }

  hash curl >/dev/null 2>&1 || {
    echo "Instalando curl ..."
    sudo apt install curl
  }

  echo "dependencias OK"
}

verificar_archlinux_deps() {

  echo "Verificando dependencias para ArchLinux ..."
  hash sudo >/dev/null 2>&1 || {
    echo "Error: sudo no está instalado"
    echo "Por favor contactar el administrador"
    echo "del equipo para proceder con la instalación"
    exit 1
  }

  sudo pacman --needed -S git curl xorg-xdpyinfo gstreamer 
}

verificar_deps() {
  if [ -e ${OS_RELEASE_FILE}  ]; then
    . ${OS_RELEASE_FILE}
    echo "Se ha detectado la distro ${PRETTY_NAME} ..."
    case "${ID}-${VERSION_ID}" in
      arch-*)
        verificar_archlinux_deps
        ;;
      manjaro-*)
        verificar_archlinux_deps
        ;;
      ubuntu-20.10*)
        verificar_debian_deps
        ;;
      ubuntu-20.04*)
        verificar_debian_deps
        ;;
      ubuntu-18.04*)
        verificar_debian_deps
        ;;
      ubuntu-17.10*)
        verificar_debian_deps
        ;;
      ubuntu-17.04*)
        verificar_debian_deps
        ;;
      ubuntu-16.10*)
        verificar_debian_deps
        ;;
      ubuntu-16.04*)
        verificar_debian_deps
        ;;
      ubuntu-15.10*)
        verificar_debian_deps
        ;;
      ubuntu-15.04*)
        verificar_debian_deps
        ;;
      ubuntu-14.10*)
        verificar_debian_deps
        ;;
      ubuntu-14.04*)
        verificar_debian_deps
        ;;
      debian-10)
        verificar_debian_deps
        ;;
      debian-9)
        verificar_debian_deps
        ;;
      debian-8)
        verificar_debian_deps
        ;;
      trisquel-8.0)
	verificar_debian_deps
	;;
      deepin-15*)
	verificar_debian_deps
        ;;
      zorin-15*)
	verificar_debian_deps
        ;;
      *)
        echo "ERROR: La distro ${PRETTY_NAME} no esta suportada" >&2
        # Aqui se deberia poder guiar el usuario hacia una instalacion manual
        exit 1
        ;;
    esac

  else
    echo "ERROR: Esta distro y/o version no esta suportada." >&2
    exit 1
  fi
}

configurar_ap(){
   if nmcli con show $1 > /dev/null 2>&1 ;then
     echo "la conexión $1 está ya instalada"
   else
     echo "creando nueva conexión $1"
     sudo nmcli con add con-name $1 ifname $WIFI_INTERFACE type wifi ssid $1 ip4 192.168.12.2/24 gw4 192.168.12.1
     sudo nmcli con modify $1 ipv4.dns "192.168.26.10"
     sudo nmcli con modify $1 connection.autoconnect no
     sudo nmcli con modify $1 wifi-sec.key-mgmt wpa-psk wifi-sec.psk $2
   fi
}

main() {
  # colores
  if which tput >/dev/null 2>&1; then
      ncolors=$(tput colors)
  fi
  if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
  fi

  # Habilitación de exit-on-error después de colores 
  set -e

  verificar_deps

  if [ ! -n "$LIBRECAST" ]; then
    LIBRECAST=~/librecast
  fi

  if [ -d "$LIBRECAST" ]; then
    printf "${YELLOW}Ya tienes libreCast instalado.${NORMAL}\n"
    printf "tienes que eliminar $LIBRECAST si quieres re-instalar.\n"
    exit
  fi


  # Control de permisos, para evitar la clonacion del repo con privilegios
  # no deseados
  umask g-w,o-w

  printf "${BLUE}Clonando libreCast...${NORMAL}\n"

  env git clone https://gitlab.softwarelibre.gob.bo/agetic/librecast/librecast-client.git $LIBRECAST || {
    printf "Error: el git clone del repo librecast ha fallado \n"
    exit 1
  }

  # esto tiene que adaptarse a diferentes distros

  mkdir -p $HOME/.local/share/applications
  cp $HOME/librecast/libreCast.desktop $HOME/.local/share/applications/libreCast-s1.desktop
  sed -i -e "s/USER/$USER_NAME/g" $HOME/.local/share/applications/libreCast-s1.desktop
  cp $HOME/.local/share/applications/libreCast-s1.desktop $HOME/.local/share/applications/libreCast-s2.desktop
  cp $HOME/.local/share/applications/libreCast-s1.desktop $HOME/.local/share/applications/libreCast-s3.desktop
  cp $HOME/.local/share/applications/libreCast-s1.desktop $HOME/.local/share/applications/libreCast-s4.desktop
  cp $HOME/.local/share/applications/libreCast-s1.desktop $HOME/.local/share/applications/libreCast-s5.desktop
  cp $HOME/.local/share/applications/libreCast-s1.desktop $HOME/.local/share/applications/libreCast-s6.desktop
  sed -i -e "s/libreCast-s1/libreCast-s2/g" $HOME/.local/share/applications/libreCast-s2.desktop
  sed -i -e "s/libreCast-s1/libreCast-s3/g" $HOME/.local/share/applications/libreCast-s3.desktop
  sed -i -e "s/libreCast-s1/libreCast-s4/g" $HOME/.local/share/applications/libreCast-s4.desktop
  sed -i -e "s/libreCast-s1/libreCast-s5/g" $HOME/.local/share/applications/libreCast-s5.desktop
  sed -i -e "s/libreCast-s1/libreCast-s6/g" $HOME/.local/share/applications/libreCast-s6.desktop

  if which nmcli >/dev/null 2>&1; then
    # esto tiene que cambiar
    # y jalar un archivo .yaml
    # de configuracion
    configurar_ap libreCast-s0 $AP_PSK
    configurar_ap libreCast-s1 $AP_PSK
    configurar_ap libreCast-s2 $AP_PSK
    configurar_ap libreCast-s3 $AP_PSK
    configurar_ap libreCast-s4 $AP_PSK
    configurar_ap libreCast-s5 $AP_PSK
    configurar_ap libreCast-s6 $AP_PSK
  else
    echo "Advertencia: nmcli no está instalado"
    echo "Será necesario configurar la conexión wifi manualmente"
    echo "Consultar la guía de instalación para mayores info"
    echo "https://gitlab.softwarelibre.gob.bo/agetic/librecast/librecast-client/-/blob/master/INSTALL.md"
  fi

  printf "${GREEN}"
  echo '    ___ __              ______           __  '
  echo '   / (_) /_  ________  / ____/___ ______/ /_ '
  echo '  / / / __ \/ ___/ _ \/ /   / __ `/ ___/ __/ '
  echo ' / / / /_/ / /  /  __/ /___/ /_/ (__  ) /_   '
  echo '/_/_/_.___/_/   \___/\____/\__,_/____/\__/   '
  echo ''
  echo '                        ...ha sido instalado!'
  echo ''
  echo ''
  printf "${NORMAL}"
}

main

