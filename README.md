# libreCast

Transmisión inalámbrica de pantalla basada en GStreamer

## Instalación Básica

Es suficiente copiar y pegar el siguiente comando en tu terminal.

```sh
sh -c "$(wget https://gitlab.softwarelibre.gob.bo/agetic/librecast/librecast-client/-/raw/master/tools/install.sh -O -)"
```

Para una guia detallada de instalación puedes consultar la [guia de instalación](INSTALL.md).

## Problemas Conocidos

# Incompatibilidad con Wayland

El cliente libreCast esta' basado en el modulo `ximagesrc` de gstreamer para captura de pantalla.

Esto implica que actualmente las sesiones gráficas basadas en Wayland no van a poder funcionar con libreCast.

Es un problema difundido en la mayoría de los capturadores de pantallas diseñados para Xorg.

Actualmente el problema se verifica por ejemplo en `Debian 10`. La solución en este caso es hacer login con las opciones avanzadas de y eligir la opción Xorg para realizar el acceso a su sistema.
