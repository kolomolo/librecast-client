# Instalación

## Requerimientos

El cliente de **libreCast** esta desarrollado para sistemas operativos **Linux**.
Se ha verificado el funcionamiento del cliente en las siguientes distros:

* Arch Linux
* Manjaro Linux
* Debian 8
* Debian 9
* Debian 10 (con Xorg)
* Ubuntu 16.04
* Ubuntu 18.04
* Ubuntu 20.04
* Ubuntu 20.10
* Trisquel 8.0
* Zorin 15

En fase de instalación será necesario disponer de la herramienta `sudo` y de los privilegios oportunos
para instalar paquetes.

En particular el instalador los guiará en la instalación de los paquetes:

* git
* gstreamer1.0-tools
* curl


## Script de instalación

Para instalar el cliente de **libreCast** es suficiente copiar y pegar el siguiente comando en tu consola.

### via curl

```shell
sh -c "$(curl -fsSL https://gitlab.softwarelibre.gob.bo/agetic/librecast/librecast-client/-/blob/master/INSTALL.md)"
```

### via wget

```sh
sh -c "$(wget https://gitlab.softwarelibre.gob.bo/agetic/librecast/librecast-client/-/blob/master/INSTALL.md -O -)"
```

# Uso

## Iniciar la proyección

Para iniciar la proyección inalámbrica es suficiente entrar a tu buscador de aplicaciones y buscar la app libreCast

Por ejemplo en GNOME3 se visualizará un menu del siguiente tipo

![gnome3](img/gnome3-menu-small.png)

Será suficiente hacer click sobre el icono de libreCast, en este ejemplo hay dos salas disponibles: libreCast-s1, libreCast-s0

## Terminar la proyección

Para terminar la proyección será suficiente repetir el paso anterior, haciendo click sobre el mismo icono se detendrá la transmisión.


