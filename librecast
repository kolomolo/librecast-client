#!/usr/bin/env bash

valid_ip() {
  local  ip=$1
  local  stat=1

  if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
      OIFS=$IFS
      IFS='.'
      ip=($ip)
      IFS=$OIFS
      [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
          && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
      stat=$?
  fi
  return $stat
}

invocar_nmcli() {
  echo "conectando a $AP_NAME"
  nmcli con up $AP_NAME
  if [ $? -eq 1 ];then
    exit 1
  fi
}

invocar_pacmd() {
  pacmd unload-module module-null-sink
  pacmd load-module module-null-sink sink_name=libreCast
  pacmd update-sink-proplist libreCast device.description=libreCast
  pacmd set-default-sink libreCast
}

invocar_gst() {

  SIZE_STRING=$(xdpyinfo | awk '/dimensions/{print $2}')
  SCREEN_WIDTH="$(cut -d'x' -f1 <<<"$SIZE_STRING")"
  SCREEN_HEIGHT="$(cut -d'x' -f2 <<<"$SIZE_STRING")"
  # por defecto no realizamos ningun recorte
  CROP_STRING_X=""
  CROP_STRING_Y=""

  # en caso el ancho supere lo maximo consentido de 1920
  if [ "$SCREEN_WIDTH" -gt 1920 ];then
    echo "recortando el ancho de la pantalla ..."
    SCREEN_WIDTH=1920
    CROP_STRING_X="startx=0 endx=1919"
    CROP_STRING_Y="starty=0 endy=$(( $SCREEN_HEIGHT-1 ))"
  fi

  # en caso el alto supere lo maximo consentido de 1080
  if [ "$SCREEN_HEIGHT" -gt 1080 ];then
    echo "recortando el alto de la pantalla..."
    SCREEN_HEIGHT=1080
    CROP_STRING_X="startx=0 endx=$(( $SCREEN_WIDTH-1 ))"
    CROP_STRING_Y="starty=0 endy=1079"
  fi

  video_gst="gst-launch-1.0 ximagesrc use-damage=false $CROP_STRING_X $CROP_STRING_Y ! videoconvert ! videoscale ! video/x-raw,format=I420,width=$SCREEN_WIDTH,height=$SCREEN_HEIGHT,framerate=25/1 ! jpegenc quality=80 ! rtpjpegpay ! udpsink host=$AP_ADDRESS port=1234 sync=false"
  audio_gst="gst-launch-1.0 pulsesrc device=libreCast.monitor ! audio/x-raw,rate=11500,channels=2,depth=16 ! udpsink host=$AP_ADDRESS port=5678 sync=false"

  if [ "$CODEC" == "h264" ] ;then
    video_gst="gst-launch-1.0 ximagesrc use-damage=false $CROP_STRING_X $CROP_STRING_Y ! videoconvert ! videoscale ! video/x-raw,format=I420,width=$SCREEN_WIDTH,height=$SCREEN_HEIGHT,framerate=25/1 ! vaapih264enc ! rtph264pay config-interval=1 pt=96 ! udpsink host=$AP_ADDRESS port=1234 sync=false"
  fi

  $video_gst &
  $audio_gst &
}

main() {
  if [ -z "$(pidof gst-launch-1.0)" ] ;then

    pkill -15 gst-launch-1.0

    # AP_NAME es necesario solo si se quiere usar Network Manager
    # para la conexion automatica al AP wifi  
    if [ -n "$AP_NAME" ] ;then
      if which nmcli >/dev/null 2>&1 ; then
        invocar_nmcli
       else
        echo "utilizando conexion manual"
      fi
    fi


    curl --connect-timeout 3 -s -k -X GET https://$AP_ADDRESS:5443/player/start?codec=$CODEC | grep 'Player iniciado'
    if [ $? -eq 0 ];then
      invocar_pacmd
      invocar_gst
      exit 0
    fi

  else

    echo "stopping ..."
    curl --connect-timeout 3 -s -k -X GET https://$AP_ADDRESS:5443/player/stop
    pkill -15 gst-launch-1.0
    pacmd unload-module module-null-sink

    if [ -n "$AP_NAME" ] ;then
      if which nmcli >/dev/null 2>&1 ; then
         nmcli con down $AP_NAME
       else
         echo "Por favor desconectarse de $AP_NAME"
      fi
    fi
  fi
}

# por defecto apuntamos a la ip predefinida del AP wifi
AP_ADDRESS=192.168.12.1
CODEC="mjpeg"

if [ $# -eq 1 ];then
  if valid_ip $1;then
    AP_ADDRESS=$1
  else
    AP_NAME=$1
  fi
fi

if [ $# -eq 2 ];then

  AP_ADDRESS=$1
  case "$2" in
    h264) CODEC=h264;;
    mjpeg) CODEC=mjpeg;;
    *) AP_NAME=$2;;
  esac;

fi

echo "enviando a la direccion $AP_ADDRESS"
echo "adoptando codec $CODEC"

main
